FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        at-spi2-core \
        binutils \
        gtk2-engines-murrine \
        libasound2 \
        libatk1.0-0 \
        libc6 \
        libcairo2 \
        libcanberra-gtk-module \
        libcups2 \
        libdbus-glib-1-2 \
        libgconf-2-4 \
        libgcrypt20 \
        libgdk-pixbuf2.0-0 \
        libgtk-3-0 \
        libice6 \
        libncurses5 \
        unzip \
        cmake \
        make \
        build-essential \
        g++

# Download and install CCS
RUN apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
    && mkdir -p /x \
    && curl -L https://software-dl.ti.com/ccs/esd/CCSv10/CCS_10_2_0/exports/CCS10.2.0.00009_linux-x64.tar.gz | tar xvz --strip-components=1 -C /x \
    && apt-get remove -y \
        ca-certificates \
        curl

RUN /x/ccs_setup_10.2.0.00009.run --mode unattended --prefix /opt/ti/ --debuglevel 4 --enable-components PF_MSP430
RUN rm -rf /x/
RUN echo "/opt/ti/ccs/eclipse" > /etc/environment

WORKDIR /opt/ti

RUN ls -1a /opt
RUN ls -1a /opt/ti
